package com.fujitsu.ca.fic.caissepop.evaluation;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.pigunit.PigTest;
import org.apache.pig.tools.parameters.ParseException;
import org.junit.Ignore;
import org.junit.Test;

public class CosineSimilarityTest {

    @Test
    public void testGivenTwoBagsShouldReturnCosineValue() throws IOException, ParseException {
        List<Tuple> relation1 = new ArrayList<Tuple>();
        relation1.add(TupleFactory.getInstance().newTuple(new Long(1)));
        relation1.add(TupleFactory.getInstance().newTuple(new Long(2)));
        relation1.add(TupleFactory.getInstance().newTuple(new Long(3)));

        List<Tuple> relation2 = new ArrayList<Tuple>();
        relation2.add(TupleFactory.getInstance().newTuple(new Long(2)));
        relation2.add(TupleFactory.getInstance().newTuple(new Long(3)));
        relation2.add(TupleFactory.getInstance().newTuple(new Long(4)));
        DataBag relation1Bag = BagFactory.getInstance().newDefaultBag(relation1);
        DataBag relation2Bag = BagFactory.getInstance().newDefaultBag(relation2);

        CosineSimilarity cosine = new CosineSimilarity();

        Tuple input = TupleFactory.getInstance().newTuple();
        input.append(relation1Bag);
        input.append(relation2Bag);
        Double result = cosine.exec(input);

        assertThat(2.0 / 3, equalTo(result));
    }

    @Ignore
    public void testGivenTwoBagsShouldReturnExpectedPigUnitTest() throws IOException,
            ParseException {
        String[] params = { "INPUT=data/test/cosine-data-1.txt", "OUTPUT=data/out/test/cosine" };
        String[] expected = { "(0.66666666666)" };

        PigTest pigTest = new PigTest("pig/compute_tfidf-test.pig", params);

        pigTest.assertOutput("out", expected);
    }
}
