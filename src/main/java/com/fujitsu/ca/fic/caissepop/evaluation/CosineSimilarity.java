package com.fujitsu.ca.fic.caissepop.evaluation;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;

import com.google.common.collect.Sets;

public class CosineSimilarity extends EvalFunc<Double> {

    @Override
    public Double exec(Tuple input) throws IOException {
        DataBag relation = (DataBag) input.get(0);
        DataBag candidate_relation = (DataBag) input.get(1);

        Set<Long> relationSet = new TreeSet<Long>();
        for (Tuple index : relation) {
            relationSet.add((Long) index.get(0));
        }

        Set<Long> relationCandidateSet = new TreeSet<Long>();
        for (Tuple index : candidate_relation) {
            relationCandidateSet.add((Long) index.get(0));
        }

        Set<Long> intersection = Sets.intersection(relationSet, relationCandidateSet);

        return cosine(relationSet.size(), relationCandidateSet.size(), intersection.size());
    }

    double cosine(int sizeA, int sizeB, int sizeInter) {
        return sizeInter / Math.sqrt((double) sizeA * sizeB);
    }
}
